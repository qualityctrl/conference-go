from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    r = requests.get(url, headers=headers)

    q = json.loads(r.content)
    picture = q["photos"][0]["src"]["original"]

    return picture


def get_weather_data(city, state):
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    r = requests.get(url)

    p = json.loads(r.content)
    if p["cod"] == "404":
        return None
    else:

        print(p)
        temperature = p["main"]["temp"]
        description = p["weather"][0]["description"]
        dictionary = {"temp": temperature, "description": description}
        return dictionary
